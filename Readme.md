# Revealjs slideshow template

A template for writing slideshows in markdown and transform them into html or 
pdf.

## Usage 

* Install dependencies :
  * [pandoc](https://pandoc.org/) for html output
  * [decktape](https://github.com/astefanutti/decktape) for pdf output. 
    
* Check that you have the proper fonts : [`Poppins 
  semibold`](https://fonts.google.com/specimen/Poppins)
  & [`Carlito`](https://www.1001fonts.com/carlito-font.html) (or [`Lato`](https://fonts.google.com/specimen/Lato))

* Write your presentation in pandoc using [pandoc markdown 
  syntax](https://pandoc.org/MANUAL.html#slide-shows) (default 
  `presentation.md` file shows some examples)

* Convert to a [revealjs](https://revealjs.com/) html presentation with
  `make render_html` (or just `make`) or to a pdf with `make render_pdf`.

## Customization

* The name of the presentation and of the output can be changed with 
  environment variables `IN` and `OUT`. `IN` is the markdown source file, and 
  `OUT` is the basename (no extension) of the output files. They can be 
  updated in the header of the "Makefile" accordingly, or passed to the 
  command line as in the following example :
  `make render_html OUT=$(date -I)_presentation`.
* Adding and positionning the logo of the customer on the title page can be 
  done using the yaml header of the markdown file. 

## Screenshots

![Screenshot 1](images/screen1.png)
![Screenshot 2](images/screen2.png)

## Gitlab pages

Build and deployed to Gitlab pages with Gitlab CI : [Demo 
link](https://multi-coop.gitlab.io/slider-template)
