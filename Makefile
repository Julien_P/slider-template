.PHONY: all, render

IN ?= presentation.md
OUT ?= $(basename $(IN))

all: render_html

render_html: ## Render presentation to revealjs slideshow with pandoc
	@pandoc \
		-t revealjs \
		-o $(OUT).html \
		-V colorlinks=true \
		-V theme=white \
		-V slideNumber=true \
		-V progress=true \
		--css static/style.css \
		--standalone \
		--slide-level=2 \
		--include-in-header=static/header_include.html \
		$(IN)

$(OUT).html: render_html

render_pdf: $(OUT).html ## Render presentation to pdf with decktape (requires revealjs slideshow)
	decktape $(OUT).html $(OUT).pdf

gitlab_pages: render_html # Used in CI to prepare ./public directory for gitlab pages
	@mkdir public
	@echo '/slider-template  /slider-template/$(OUT).html' > public/_redirects
	@cp $(OUT).html public
	@cp -r static public/static
	@cp -r images public/images

help:
		@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
